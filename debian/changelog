libperlio-gzip-perl (0.20-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 01 Apr 2023 03:18:13 +0000

libperlio-gzip-perl (0.20-1) unstable; urgency=medium

  [ Florian Schlichting ]
  IGNORE-VERSION: 0.20-1
  no relevant code changes

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Florian Schlichting ]
  * Import upstream version 0.20
  * Add upstream metadata

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 00:01:30 +0100

libperlio-gzip-perl (0.19-1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 21 Apr 2021 11:31:46 +0200

libperlio-gzip-perl (0.19-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * New upstream release.
  * Drop PerlIO-gzip-0.18-RT92412.patch which was taken from CPAN RT and
    is included now.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Mon, 09 Nov 2015 21:29:41 +0100

libperlio-gzip-perl (0.18-3) unstable; urgency=medium

  * Team upload.
  * Add patch from CPAN RT to handle %Config changes in perl 5.20.
    (Closes: #750237)

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Jun 2014 17:46:15 +0200

libperlio-gzip-perl (0.18-2) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ intrigeri ]
  * Bump debhelper compat level to 9.
  * Adjust versioned Build-Depends on debhelper to (>= 9.20120312~)
    to get hardening build flags.
  * Declare compatibility with Standards-Version 3.9.5.

 -- intrigeri <intrigeri@debian.org>  Sun, 09 Mar 2014 03:53:10 +0100

libperlio-gzip-perl (0.18-1) unstable; urgency=low

  * Initial Release (Closes: #618272)

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 13 Mar 2011 19:40:29 -0400
